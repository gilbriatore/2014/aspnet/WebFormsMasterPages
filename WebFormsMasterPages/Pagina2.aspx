﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMestre.Master" 
    AutoEventWireup="true" CodeBehind="Pagina2.aspx.cs" 
    Inherits="WebFormsMasterPages.Pagina2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="conteudo" runat="server">
    <p>Listagem de pessoas</p>
    <p>
        <asp:Button ID="btnCarregar" runat="server" OnClick="btnCarregar_Click" Text="Carregar" />
    </p>
    <p>&nbsp;</p>
    <p>
        <asp:GridView ID="grdView" runat="server">
        </asp:GridView>
    </p>
    <p>&nbsp;</p>
<p>
    <asp:HyperLink ID="HyperLink1" runat="server" 
        NavigateUrl="~/Default.aspx">Voltar para página inicial</asp:HyperLink>
</p>
</asp:Content>
