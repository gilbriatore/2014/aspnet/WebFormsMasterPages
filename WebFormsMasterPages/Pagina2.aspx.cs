﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dominio;

namespace WebFormsMasterPages
{
    public partial class Pagina2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCarregar_Click(object sender, EventArgs e)
        {
            Pessoa p1 = new Pessoa();
            p1.Nome = "João";
            p1.Sobrenome = "Silva";

            Pessoa p2 = new Pessoa();
            p2.Nome = "Maria";
            p2.Sobrenome = "Aparecida";

            List<Pessoa> lista = new List<Pessoa>();
            lista.Add(p1);
            lista.Add(p2);

            grdView.DataSource = lista;
            grdView.DataBind();

        }
    }
}